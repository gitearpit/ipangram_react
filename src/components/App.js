import React, { useContext } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Layout from "./public/Layout";
import Login from "./public/Login";
import Register from "./public/Register";

import PrivateLayout from "./private/Layout";
import Dashboard from "./private/Dashboard";
import { UserContext } from "./shared_components/Context";
import Employees from "./private/Employees";
import Department from "./private/Department";

export default function App() {

  const { user } = useContext(UserContext);

  return (
    <BrowserRouter>
      <Routes>

        <Route path="/" element={!user ? <Layout /> : <Navigate to="/auth" />}>
          <Route index element={<Login />} />
          <Route path="register" element={<Register />} />
        </Route>

        <Route path="/auth" element={user ? <PrivateLayout /> : <Navigate to="/" />}>
          <Route index element={<Dashboard />} />
          <Route path="dashboard" element={<Dashboard />} />
          <Route path="employees" element={<Employees />} />
          <Route path="department" element={<Department />} />
        </Route>

      </Routes>
    </BrowserRouter>
  )
}
