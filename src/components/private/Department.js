import React, { useContext, useEffect, useState } from 'react'
import ComponentModal from '../shared_components/ComponentModal'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import ApiService from '../../helpers/api_service';
import { UserContext } from '../shared_components/Context';
import { Toaster, toast } from 'react-hot-toast';
import { formValidationFieldRequire } from '../../helpers/common_functions';

export default function Department() {

  const { token } = useContext(UserContext);
  const header = {
    "Authorization": `Bearer ${token}`
  }
  const [department, setDepartment] = useState([]);
  const departmentInitialState = {
    name: '',
    category: '',
    employees: [],
    location: '',
    salary: ''
  }
  const [departmentData, setDepartmentData] = useState(departmentInitialState);
  const [usersOption, setUsersOption] = useState([]);
  const [modalState, setModalState] = useState(false);
  const [departmentMaster, setDepartmentMaster] = useState([]);

  const closeModal = () => {
    setModalState(!modalState);
    setDepartmentData(departmentInitialState);
  };

  const columns = [
    {
      dataField: '_id',
      text: 'Department ID'
    },
    {
      dataField: 'category.label',
      text: 'Category',
    },
    {
      dataField: 'name.label',
      text: 'Department',
    },
    {
      dataField: 'employees',
      text: 'Employees',
      formatter: (row, data, index) => {
        if (data?.employees?.length) {
          return data?.employees.map((employee, i) => {
            return (
              <span key={i}>
                {employee?.label}
                <br />
              </span>
            )
          })
        }
        else {
          return "-";
        }
      }
    },
    {
      dataField: 'salary',
      text: 'Salary',
    },
    {
      dataField: 'location.label',
      text: 'Location',
    },
    {
      dataField: 'price',
      text: 'Action',
      formatter: (row, data, index) => {
        return (
          <>
            <button
              className='btn btn-primary me-1'
              onClick={e => {
                setModalState(true);
                setDepartmentData(data);
              }}
            >
              Edit
            </button>
            <button className='btn btn-danger' onClick={e => deleteDepartment(data._id)} >Delete</button>
          </>
        )
      }
    }
  ];

  useEffect(() => {
    fetchAllDepartment();
    fetchAllUnassignedEmployee();
    fetchDepartmentMaster();
  }, [])

  const fetchAllUnassignedEmployee = () => {
    ApiService.get('/users/get_unassigned', header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          const options = result?.data?.map(e => ({ value: e._id, label: `${e.firstName} ${e.lastName}` }));
          setUsersOption(options);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  const fetchDepartmentMaster = () => {
    ApiService.get('/department/get-master', header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          setDepartmentMaster(result?.data);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  const fetchAllDepartment = () => {
    ApiService.get('/department/get', header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          setDepartment(result?.data)
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  const deleteDepartment = (id) => {
    const confirm = window.confirm('Are you sure you want to delete this employee.')
    if (confirm) {
      ApiService.delete(`/department/delete/${id}`, header)
        .then(res => {
          const result = res?.data;
          if (result?.success) {
            toast.success(result?.message);
            fetchAllDepartment();
            fetchAllUnassignedEmployee();
          }
        })
        .catch(error => {
          toast.error(error?.response?.data?.message);
        });
    }
  }

  const updateDepartmentData = (e, type) => {
    const key = e?.target?.name;
    const value = e?.target?.value;
    // if (type === "react_select") {
    //   setDepartmentData(prev => ({ ...prev, 'employees': e }))
    //   return
    // }
    setDepartmentData(prev => ({ ...prev, [key]: value }))
  }

  const saveUpdates = () => {
    const valid = formValidationFieldRequire(departmentData);
    if (!valid.status) {
      toast.error(valid.message);
      return
    }
    ApiService.put(`/department/update/${departmentData?._id}`, departmentData, header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          toast.success(result.message);
          closeModal();
          fetchAllDepartment();
          fetchAllUnassignedEmployee();
        }
        else {
          toast.error(result.message);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  const saveNewDepartment = () => {
    const valid = formValidationFieldRequire(departmentData);
    if (!valid.status) {
      toast.error(valid.message);
      return
    }
    ApiService.post(`/department/create`, departmentData, header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          toast.success(result.message);
          closeModal();
          fetchAllDepartment();
          fetchAllUnassignedEmployee();
        }
        else {
          toast.error(result.message);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  return (
    <>
      <button
        className='btn btn-primary m-1 float-end'
        onClick={() => setModalState(true)}
      >
        Add Department
      </button >

      <BootstrapTable
        keyField='_id'
        data={department}
        columns={columns}
        pagination={paginationFactory({
          sizePerPageList: [{
            text: '5', value: 5
          }, {
            text: '10', value: 10
          }, {
            text: 'All', value: department.length
          }]
        })}
      />

      <ComponentModal
        modalState={modalState}
        closeModal={closeModal}
        modalheading="Add Department"
        id="department_form"
        localData={departmentData}
        updateLocalData={(e, type) => updateDepartmentData(e, type)}
        saveData={(departmentData?._id) ? saveUpdates : saveNewDepartment}
        selectOption={{ usersOption, departmentMaster }}
      />
      <Toaster />
    </>
  )
}
