import React, { useContext, useEffect, useState } from 'react'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { UserContext } from '../shared_components/Context';
import ApiService from '../../helpers/api_service';
import { Toaster, toast } from 'react-hot-toast';
import ComponentModal from '../shared_components/ComponentModal';
import { formValidationFieldRequire } from '../../helpers/common_functions';
import { Form } from 'react-bootstrap';

export default function Employees() {

  const { token } = useContext(UserContext);
  const header = {
    "Authorization": `Bearer ${token}`
  }
  const [users, setUsers] = useState([]);
  const [userData, setUserData] = useState({});
  const [modalState, setModalState] = useState(false);

  const closeModal = () => { setModalState(!modalState) };
  const columns = [
    {
      dataField: 'employeeId',
      text: 'Employee ID'
    },
    {
      dataField: 'firstName',
      text: 'Employee Name',
      formatter: (row, data, index) => `${data.firstName} ${data.lastName}`,
    },
    {
      dataField: 'department',
      text: 'Department',
      formatter: (row, data, index) => (data?.department) ? data?.department?.label : "-"
    },
    {
      dataField: 'category?.label',
      text: 'Category',
      formatter: (row, data, index) => (data?.category) ? data?.category?.label : "-"
    },
    {
      dataField: 'location?.label',
      text: 'Location',
      formatter: (row, data, index) => (data?.location) ? data?.location?.label : "-"
    },
    {
      dataField: 'salary',
      text: 'Salary',
      formatter: (row, data, index) => (data?.salary) ? data?.salary : "-"
    },
    {
      dataField: 'action',
      text: 'Action',
      formatter: (row, data, index) => {
        return (
          <>
            <button
              className='btn btn-primary me-1'
              onClick={e => {
                setModalState(true);
                setUserData(data);
              }}
            >
              Edit
            </button>
            <button className='btn btn-danger' onClick={e => deleteEmployee(data._id)} >Delete</button>
          </>
        )
      }
    }
  ];

  useEffect(() => {
    fetchAllUsers();
  }, [])

  const fetchAllUsers = (data) => {
    ApiService.post('/users/get', data, header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          setUsers(result?.data)
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  const updateUserData = (e, type) => {
    const key = e.target.name;
    const value = e.target.value;
    setUserData(prev => ({ ...prev, [key]: value }))
  }

  const deleteEmployee = (id) => {
    const confirm = window.confirm('Are you sure you want to delete this employee.')
    if (confirm) {
      ApiService.delete(`/users/delete/${id}`, header)
        .then(res => {
          const result = res?.data;
          if (result?.success) {
            toast.success(result?.message);
            fetchAllUsers();
          }
        })
        .catch(error => {
          toast.error(error?.response?.data?.message);
        });
    }
  }

  const saveUpdates = () => {
    const valid = formValidationFieldRequire(userData);
    if (!valid.status) {
      toast.error(valid.message);
      return
    }
    ApiService.put(`/users/update/${userData?._id}`, userData, header)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          toast.success(result.message);
          closeModal();
          fetchAllUsers();
        }
        else {
          toast.error(result.message);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  return (
    <>
      <BootstrapTable
        keyField='_id'
        data={users}
        columns={columns}
        pagination={paginationFactory({
          sizePerPageList: [{
            text: '5', value: 5
          }, {
            text: '10', value: 10
          }, {
            text: 'All', value: users.length
          }]
        })}
      />

      <div key={`inline-radio`} className="mb-3 ms-3">
        <Form.Check
          label="To fetch user in IT and location name start with A."
          name="group1"
          type="radio"
          onClick={e => fetchAllUsers({ dataFor: "IT" })}
          id={`inline-radio-1`}
        />
        <Form.Check
          label="To fetch users in Sales in descending ordet to their names."
          name="group1"
          type="radio"
          onClick={e => fetchAllUsers({ dataFor: "Sales" })}
          id={`inline-radio-2`}
        />
      </div>

      <ComponentModal
        modalState={modalState}
        closeModal={closeModal}
        modalheading="Update Employee"
        id="employee_form"
        localData={userData}
        updateLocalData={(e, type) => updateUserData(e, type)}
        saveData={saveUpdates}
      />
      <Toaster />
    </>
  )
}
