import React, { useContext } from 'react'
import { UserContext } from '../shared_components/Context';

export default function Dashboard() {

  const { user } = useContext(UserContext);

  return (
    <>
      <div className="container-fluid px-4">
        <h1 className="mt-4">User Profile</h1>

        <div className='border mt-3 p-4 progress-bar'>
          <div className='row'>
            <div className='col'>
              <b>Name</b>
            </div>
            <div className='col'>
              <p>{`${user?.firstName} ${user?.lastName}`}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Email</b>
            </div>
            <div className='col'>
              <p>{user?.email}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Employee Id</b>
            </div>
            <div className='col'>
              <p>{user?.employeeId}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Role</b>
            </div>
            <div className='col'>
              <p>{user?.role}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Department</b>
            </div>
            <div className='col'>
              <p>{user?.department?.label || '-'}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Category</b>
            </div>
            <div className='col'>
              <p>{user?.category?.label || '-'}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Salary</b>
            </div>
            <div className='col'>
              <p>{user?.salary || '-'}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Location</b>
            </div>
            <div className='col'>
              <p>{user?.location?.label || '-'}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Gender</b>
            </div>
            <div className='col'>
              <p>{user?.gender}</p>
            </div>
          </div>
          <hr />

          <div className='row'>
            <div className='col'>
              <b>Hobbies</b>
            </div>
            <div className='col'>
              <p>{user?.hobbies}</p>
            </div>
          </div>

        </div>
      </div>

    </>
  )
}
