import { Outlet } from "react-router-dom";
import Header from "./Header";
import Sider from "./Sider";
import Footer from "./Footer";

const PrivateLayout = () => {
  return (
    <>
      <div className="sb-nav-fixed">
        <Header />
        <div id="layoutSidenav">
          <Sider />
          <div id="layoutSidenav_content">
            <main>
              <Outlet />
            </main>
            <Footer />
          </div>
        </div>
      </div>
    </>
  )
};

export default PrivateLayout;