import React, { useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { UserContext } from '../shared_components/Context';

export default function Sider() {

  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);
  const logoutUser = () => {
    setUser();
    navigate('/');
  }

  return (
    <>
      <div id="layoutSidenav_nav">
        <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
          <div className="sb-sidenav-menu">
            <div className="nav">
              <Link className="nav-link" to="/">
                Dashboard
              </Link>
              {
                user.role === "manager" &&
                <>
                  <Link className="nav-link" to="employees">
                    Employees List
                  </Link>
                  <Link className="nav-link" to="department">
                    Department
                  </Link>
                </>
              }
              <button className="nav-link" onClick={logoutUser}>
                Logout
              </button>
            </div>
          </div>
          <div className="sb-sidenav-footer">
            <div className="small">Logged in as:</div>
            {`${user?.firstName} ${user?.lastName}`}
          </div>
        </nav>
      </div>
    </>
  )
}
