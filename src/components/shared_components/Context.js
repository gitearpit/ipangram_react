import React, { createContext, useState } from 'react';

const UserContext = createContext();

const MyContextProvider = ({ children }) => {
  const [user, setUser] = useState();
  const [token, setToken] = useState();

  const contextValues = {
    user,
    setUser,
    token,
    setToken
  }

  return (
    <UserContext.Provider value={contextValues}>
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, MyContextProvider };
