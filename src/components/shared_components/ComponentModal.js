import React from 'react'
import { Form } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Select from 'react-select';

export default function ComponentModal(props) {

  const { modalState, closeModal, modalheading, id, localData, updateLocalData, saveData, selectOption } = props;

  const renderBody = (id) => {
    switch (id) {
      case 'employee_form':
        return (
          <>
            <Form.Group className="mb-3" controlId="EmployeeFirstName">
              <Form.Label>Employee first name</Form.Label>
              <Form.Control
                type="text"
                name="firstName"
                placeholder="First Name"
                value={localData?.firstName}
                onChange={(e) => updateLocalData(e, 'text')}
                autoFocus
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="EmployeeLastName">
              <Form.Label>Employee last name</Form.Label>
              <Form.Control
                type="text"
                name="lastName"
                placeholder="Last Name"
                value={localData?.lastName}
                onChange={(e) => updateLocalData(e, 'text')}
              />
            </Form.Group>
          </>
        )

      case 'department_form':
        const departmentOption = selectOption?.departmentMaster.map(e => e.department)
        return (
          <>
            <Form.Group className="mb-3" controlId="DepartmentName">
              <Form.Label>Department Name</Form.Label>
              <Select
                defaultValue={localData?.name}
                onChange={(e) => {
                  let data = { target: { name: 'name', value: e } }
                  updateLocalData(data, 'react_select')
                }}
                options={departmentOption}
                name='name'
                autoFocus
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="DepartmentCategory">
              <Form.Label>Department Category</Form.Label>
              <Select
                defaultValue={localData?.category}
                onChange={(e) => {
                  let data = { target: { name: 'category', value: e } }
                  updateLocalData(data, 'react_select')
                }}
                options={selectOption?.departmentMaster?.[0]?.category}
                name='category'
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="DepartmentEmployees">
              <Form.Label>Department Employees</Form.Label>
              <Select
                defaultValue={localData?.employees}
                onChange={(e) => {
                  let data = { target: { name: 'employees', value: e } }
                  updateLocalData(data, 'react_select')
                }}
                options={selectOption?.usersOption}
                name='employees'
                isMulti={true}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="DepartmentLocation">
              <Form.Label>Department Location</Form.Label>
              <Select
                defaultValue={localData?.location}
                onChange={(e) => {
                  let data = { target: { name: 'location', value: e } }
                  updateLocalData(data, 'react_select')
                }}
                options={selectOption?.departmentMaster?.[0]?.location}
                name='location'
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="DepartmentSalary">
              <Form.Label>Department Salary</Form.Label>
              <Form.Control
                type="text"
                name="salary"
                placeholder="Department salary"
                value={localData?.salary}
                onChange={(e) => updateLocalData(e, 'text')}
              />
            </Form.Group>
          </>
        )

      default:
        return (
          <p>
            Please pass modal Id...
          </p>
        )
    }
  }


  return (
    <>
      <Modal show={modalState} onHide={closeModal} size='lg'>
        <Modal.Header closeButton>
          <Modal.Title>{modalheading}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {renderBody(id)}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Close
          </Button>
          <Button variant="primary" onClick={saveData}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}
