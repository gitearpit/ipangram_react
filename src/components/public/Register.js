import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { formValidationFieldRequire, validateEmail, validatePassword } from '../../helpers/common_functions';
import toast, { Toaster } from 'react-hot-toast';
import ApiService from '../../helpers/api_service';

export default function Register() {

  const navigate = useNavigate();
  const [userData, setUserData] = useState({
    firstName: "",
    lastName: "",
    hobbies: "",
    gender: "male",
    role: "employee",
    email: "",
    password: ""
  });

  const handleInputChange = (e, type) => {
    const key = e.target.name;
    const value = e.target.value;
    setUserData(prev => ({ ...prev, [key]: value }));
  }

  const userRegister = (e) => {
    e.preventDefault();
    const valid = formValidationFieldRequire(userData);
    const emailValid = validateEmail(userData.email);
    const passwordValid = validatePassword(userData.password);

    if (!valid.status) {
      toast.error(valid.message);
      return
    }
    else if (!emailValid) {
      toast.error("Please enter valid email..");
      return
    }
    else if (!passwordValid) {
      toast.error("Your password must contain minimum 8 character and 20 max.");
      return
    }
    ApiService.post('/users/register', userData)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          toast.success(result.message);
          navigate('/');
        }
        else {
          toast.error(result.message);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  return (
    <>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header">
                <h3 className="text-center font-weight-light my-4">Create Account</h3>
              </div>
              <div className="card-body">
                <form>
                  <div className="row mb-3">
                    <div className="col-md-6">
                      <div className="form-floating mb-3 mb-md-0">
                        <input
                          className="form-control"
                          id="inputFirstName"
                          type="text"
                          name="firstName"
                          value={userData?.firstName}
                          onChange={e => handleInputChange(e, "text")}
                          placeholder="Enter your first name"
                        />
                        <label htmlFor="inputFirstName">First name</label>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-floating">
                        <input
                          className="form-control"
                          id="inputLastName"
                          type="text"
                          name="lastName"
                          value={userData?.lastName}
                          onChange={e => handleInputChange(e, "text")}
                          placeholder="Enter your last name"
                        />
                        <label htmlFor="inputLastName">Last name</label>
                      </div>
                    </div>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="inputHobbies"
                      type="text"
                      name="hobbies"
                      value={userData?.hobbies}
                      onChange={e => handleInputChange(e, "text")}
                      placeholder="Study, Singing, Dancing ..."
                    />
                    <label htmlFor="inputHobbies">Hobbies</label>
                  </div>
                  <div className="form-floating mb-3">
                    <select
                      defaultValue={userData?.gender}
                      name="gender"
                      className="form-select"
                      onChange={e => handleInputChange(e, "select")}
                      aria-label="Default select example"
                    >
                      <option value={null} disabled selected>Select gender</option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                      <option value="other">Other</option>
                    </select>
                    <label htmlFor="inputGender">Gender</label>
                  </div>
                  <div className="form-floating mb-3">
                    <select
                      defaultValue={userData?.role}
                      name="role"
                      className="form-select"
                      onChange={e => handleInputChange(e, "select")}
                      aria-label="Default select example"
                    >
                      <option value={null} disabled selected>Select role</option>
                      <option value="employee">Employee</option>
                      <option value="manager">Manager</option>
                    </select>
                    <label htmlFor="inputGender">Role</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="inputEmail"
                      type="email"
                      name="email"
                      value={userData?.email}
                      onChange={e => handleInputChange(e, "email")}
                      placeholder="name@example.com"
                    />
                    <label htmlFor="inputEmail">Email address</label>
                  </div>
                  <div className="form-floating mb-3 mb-md-0">
                    <input
                      className="form-control"
                      id="inputPassword"
                      type="password"
                      name="password"
                      value={userData?.password}
                      onChange={e => handleInputChange(e, "password")}
                      placeholder="Create a password"
                    />
                    <label htmlFor="inputPassword">Password</label>
                  </div>
                  <div className="mt-4 mb-0">
                    <div className="d-grid">
                      <button className="btn btn-primary btn-block" onClick={userRegister}>
                        Create Account
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <div className="card-footer text-center py-3">
                <div className="small">
                  <Link to="/">Have an account? Go to login</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  )
}
