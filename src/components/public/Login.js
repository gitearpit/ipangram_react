import React, { useContext, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { formValidationFieldRequire, validateEmail } from '../../helpers/common_functions';
import toast, { Toaster } from 'react-hot-toast';
import ApiService from '../../helpers/api_service';
import { UserContext } from '../shared_components/Context';

export default function Login() {

  const { setUser, setToken } = useContext(UserContext);
  const navigate = useNavigate();
  const [userData, setUserData] = useState({
    email: "",
    password: ""
  });

  const handleInputChange = (e, type) => {
    const key = e.target.name;
    const value = e.target.value;
    setUserData(prev => ({ ...prev, [key]: value }));
  }

  const userLogin = (e) => {
    e.preventDefault();
    const valid = formValidationFieldRequire(userData);
    const emailValid = validateEmail(userData.email);
    if (!valid.status) {
      toast.error(valid.message);
      return
    }
    else if (!emailValid) {
      toast.error("Please enter valid email..");
      return
    }
    ApiService.post('/users/login', userData)
      .then(res => {
        const result = res?.data;
        if (result?.success) {
          toast.success(result?.message);
          setUser(result?.user);
          setToken(result?.token);
          navigate('/auth');
        }
        else {
          toast.error(result.message);
        }
      })
      .catch(error => {
        toast.error(error?.response?.data?.message);
      });
  }

  return (
    <>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-5">
            <div className="card shadow-lg border-0 rounded-lg mt-5">
              <div className="card-header">
                <h3 className="text-center font-weight-light my-4">Login</h3>
              </div>
              <div className="card-body">
                <form>
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="inputEmail"
                      type="email"
                      name='email'
                      onChange={e => handleInputChange(e, "email")}
                      value={userData?.email}
                      placeholder="name@example.com"
                    />
                    <label htmlFor="inputEmail">Email address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      className="form-control"
                      id="inputPassword"
                      type="password"
                      name='password'
                      onChange={e => handleInputChange(e, "password")}
                      value={userData?.password}
                      placeholder="Password"
                    />
                    <label htmlFor="inputPassword">Password</label>
                  </div>
                  <div className="d-flex align-items-center justify-content-between mt-4 mb-0">
                    <button className="btn btn-primary" onClick={userLogin}>
                      Login
                    </button>
                  </div>
                </form>
              </div>
              <div className="card-footer text-center py-3">
                <div className="small">
                  <Link to='register'>Need an account? Sign up!</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Toaster />
    </>
  )
}
