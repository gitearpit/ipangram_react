import { Outlet } from "react-router-dom";
import Footer from "./Footer";

const Layout = () => {
  return (
    <>
      <div className="bg-primary">
        <div id="layoutAuthentication">
          <div id="layoutAuthentication_content">
            <main>
              <Outlet />
            </main>
          </div>
          <div id="layoutAuthentication_footer">
            <Footer />
          </div>
        </div>
      </div>
    </>
  )
};

export default Layout;