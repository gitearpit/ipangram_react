import axios from "axios";

const api = axios.create({
  baseURL: 'http://localhost:4000',
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json"
  }
});

const ApiService = {
  get: (url, headers) => api.get(url, { headers }),

  post: (url, data, headers) => api.post(url, data, { headers }),

  put: (url, data, headers) => api.put(url, data, { headers }),

  delete: (url, headers) => api.delete(url, { headers }),
};

export default ApiService;
