export function formValidationFieldRequire(obj) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (obj[key] === "") {
        return { message: `Please enter ${separateCamelCase(key)}`, status: false };
      }
    }
  }
  return { message: `valid`, status: true };
}

function separateCamelCase(key) {
  const separated = key.replace(/([a-z])([A-Z])/g, '$1 $2');
  return separated.toLowerCase();
}

export function validateEmail(email) {
  if (email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }
  return false;
}

export function validatePassword(password) {
  const minLength = 8;
  const maxLength = 20;
  if (password.length < minLength || password.length > maxLength) {
    return false;
  }
  return true;
}