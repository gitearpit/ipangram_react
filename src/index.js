import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/App';

import { MyContextProvider } from './components/shared_components/Context';

import 'bootstrap/dist/css/bootstrap.min.css';
import "./styles/style.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <MyContextProvider>
      <App />
    </MyContextProvider>
  </React.StrictMode>
);